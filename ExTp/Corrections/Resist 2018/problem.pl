bC_rec([T|_], O, 0, Res):- atom_number(T, Res).

bC_rec([T|Q, O, CR, Res):- T < O, bC_rec(Q,O,NCR,NRes), O is T + NCR, atom_number(T, STR), atom_concat('+', STR ,TMP), atom_concat(NRes, TMP, Res),!.
bC_rec([T|Q, O, CR, Res):- T < O, bC_rec(Q,O,NCR,NRes), O is T - NCR, atom_number(T, STR), atom_concat(STR, '-' ,TMP), atom_concat(TMP, NRes, Res),!.

bC_rec([T|Q, O, CR, Res):- T < O, bC_rec(Q,O,NCR,NRes), CR is T+NCR, CR < O, atom_number(T, STR), atom_concat(STR, '+' ,TMP), atom_concat(TMP, NRes, Res).
bC_rec([T|Q, O, CR, Res):- T < O, bC_rec(Q,O,NCR,NRes), CR is T-NCR, atom_number(T, STR), atom_concat(STR, '-' ,TMP), atom_concat(TMP, NRes, Res).

bC(L,O,Res):-bC_Rec(L,O,0,Res).