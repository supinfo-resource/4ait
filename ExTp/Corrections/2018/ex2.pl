% 2.1

membre(X, [X|_]).
membre(X, [_|Q]):- membre(X,Q).

nombre(X):-membre(X,[1,2,3,4,5,6,7,8,9,10,11]).

resolution(X,Y,Z):-nombre(X), nombre(Y), nombre(Z), X\==Y,
    Y\==Z, Z\==X, X*X+2*X-Y+Y*Y=:=Z*Z.

% 2.2
nombre_20(X):-membre(X,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]).

croit(X,[Y]) :- not(membre(X,[Y])) , nombre_20(Y), X<Y,!.
croit(X,[Y|Z]) :- not(membre(X,[Y|Z])), nombre_20(Y), X<Y.
    
trouver([T]) :- nombre_20(T),!.
trouver([T|Q]) :- nombre_20(T), croit(T,Q), trouver(Q),!.