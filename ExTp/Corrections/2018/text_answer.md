# 4AIT TP - 2018

## Ex 1

Nous disposons de la formulation suivante:

- S’il ne pleut pas, je vais courir
- S’il pleut et que mes collègues sont disponibles, je vais manger
- Si mes collègues ne sont pas disponibles, je ne vais pas manger
- Donc soit je vais courir, soit je vais manger

### La formule est-elle satisfiable ?

Formalisation du problème :

```OCaml
Il pleut = P. Je vais courir = C. Mes collègues sont dispo = DC. Je vais manger = M.

Traduction des 4 formules:
(¬P => C ∧ P∧DC => M ∧ ¬DC => ¬M) => C XOR M
```

Passons par une table de vérité:

| P   | C   | DC  | M   | ¬P => C | P∧DC => M | ¬DC => ¬M | C XOR M | Possible ?                             |
|-----|-----|-----|-----|---------|-----------|-----------|---------|----------------------------------------|
| 0   | 0   | 0   | 0   | 0       |           |           |         | Impossible, ne satisfait pas ¬P => C   |
| 0   | 0   | 0   | 1   | 0       |           |           |         | Impossible, ne satisfait pas ¬P => C   |
| 0   | 0   | 1   | 0   | 0       |           |           |         | Impossible, ne satisfait pas ¬P => C   |
| 0   | 0   | 1   | 1   | 0       |           |           |         | Impossible, ne satisfait pas ¬P => C   |
| 0   | 1   | 0   | 0   | 1       | 1         | 0         |         | Impossible, ne satisfait pas ¬DC => ¬M |
| 0   | 1   | 0   | 1   | 1       | 1         | 1         | 0       |                                        |
| 0   | 1   | 1   | 0   | 1       | 1         | 1         | 1       |                                        |
| 0   | 1   | 1   | 1   | 1       | 1         | 1         | 0       |                                        |
| 1   | 0   | 0   | 0   | 1       | 1         | 1         | 0       |                                        |
| 1   | 0   | 0   | 1   | 1       | 1         | 0         |         | Impossible, ne satisfait pas ¬DC => ¬M |
| 1   | 0   | 1   | 0   | 1       | 0         | 1         |         | Impossible, ne satisfait pas P∧DC => M |
| 1   | 0   | 1   | 1   | 1       | 1         | 1         | 1       |                                        |
| 1   | 1   | 0   | 0   | 1       | 1         | 1         | 1       |                                        |
| 1   | 1   | 0   | 1   | 1       | 1         | 0         |         | Impossible, ne satisfait pas ¬DC => ¬M |
| 1   | 1   | 1   | 0   | 1       | 0         |           |         | Impossible, ne satisfait pas P∧DC => M |
| 1   | 1   | 1   | 1   | 1       | 1         | 1         | 0       |                                        |

Via la table de vérité, nous pouvons voir que plusieurs situation nous rendent incapable de prendre une décision. (Les cas où C XOR M est faux). La formule est donc satisfiable pour 3 cas particulier, et globalement, elle est insatisfiable.

### Est-il vrai que `¬(u ∨ t) = ¬u ∧ ¬t` ? Démontrez

L'équalité est vrai.

Preuve :

```OCaml
Soit u = faux
 ¬(u ∨ t) = ¬(FAUX ∨ t) = ¬t
¬u ∧ ¬t = FAUX ∧ ¬t = ¬t
=> ¬(u ∨ t) = ¬u ∧ ¬t

Soit u = vrai
 ¬(u ∨ t) = ¬(VRAI ∨ t) = ¬VRAI = FAUX
 ¬u ∧ ¬t = ¬VRAI ∧ ¬t = FAUX ∧ ¬t = FAUX
=> ¬(u ∨ t) = ¬u ∧ ¬t

Donc l'égalité est vrai.
```

## Ex 3.2

Il y a 96 possibles (listé via la méthode assemblage du programme `ex3.pl`)