membre(X,[X|_]).
membre(X,[_|Z]) :- membre(X,Z).

longueur(0 ,[]).
longueur(N ,[_|Q]) :- longueur(M,Q) , N is M + 1.
    
assemblage(X):- assemblage([],X).
assemblage(X,X):-longueur(8,X),!,verifier(X).
assemblage(X,Solution):- membre(Couleur,[orange,bleu,vert,jaune]),
                        membre(Forme, [carre,rond]), not(membre(forme(Forme,Couleur),X)),
                        assemblage([forme(Forme,Couleur)|X],Solution).


verifier([X|Liste]):- verifier(X,[X|Liste]).
verifier(Premier,[Dernier]):- compatibles(Premier,Dernier).
verifier(Premier,[X,Y|Reste]):- compatibles(X,Y), verifier(Premier,[Y|Reste]).

compatibles(forme(Forme1,Couleur1), forme(Forme2,Couleur2)):-Couleur1 \= Couleur2, Forme1 \= Forme2.